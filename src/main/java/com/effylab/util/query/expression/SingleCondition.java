/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import com.effylab.util.query.predicate.PredicateBuilder;
import com.effylab.util.query.predicate.PredicateFactory;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 22, 2014
 * <p>
 * @version 1.0.2
 */
public class SingleCondition implements Condition {

    private static final String FIELD_NAME_PATTERN;

    private static final String VALUE_PATTERN;

    private static final Pattern EXPRESSION_STATEMENT_PATTERN;

    static {
        FIELD_NAME_PATTERN = "[\\w\\$\\.]+";
        String valueSymbols = "\\w\\$\\-\\.А-Яа-я@:;\\\\\\/";
        VALUE_PATTERN = "'[" + valueSymbols + "\\s\"\\']*'|\"[" + valueSymbols + "\\s\"\\']*\"|[" + valueSymbols + "]*";
        EXPRESSION_STATEMENT_PATTERN = Pattern.compile("(?<name>" + FIELD_NAME_PATTERN + ")" +
                                                       "(?<operator>" + Operator.getRegularExpressions("|") + ")" +
                                                       "(?<value>" + VALUE_PATTERN + ")?");
    }

    public static Pattern getExpressionStatementPattern () {
        return EXPRESSION_STATEMENT_PATTERN;
    }

    public static SingleCondition parse (String expression) {
        if (expression == null) {
            throw new IllegalArgumentException("Filter expression string is null");
        }
        String trimmedExpression = expression.trim();
        if (trimmedExpression.isEmpty()) {
            throw new IllegalArgumentException("Filter expression string is empty");
        }
        Matcher matcher = EXPRESSION_STATEMENT_PATTERN.matcher(trimmedExpression);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Filter expression string, \"" + trimmedExpression +
                                               "\", doesn't match pattern");
        }

        return new SingleCondition(matcher.group("name"),
                                   matcher.group("operator"),
                                   matcher.group("value"));
    }

    private final String fieldName;

    private final Operator operator;

    private final String value;

    public SingleCondition (String fieldName, String operator, String value) {
        this(fieldName, Operator.parse(operator), value);
    }

    public SingleCondition (String fieldName, String operator) {
        this(fieldName, Operator.parse(operator));
    }

    public SingleCondition (String fieldName, Operator operator) {
        this(fieldName, operator, "");
    }

    public SingleCondition (String fieldName, Operator operator, String value) {
        this.fieldName = fieldName;
        this.operator = operator;

        assert value != null;
        this.value = (value.startsWith("'") && value.endsWith("'")) || (value.startsWith("\"") && value.endsWith("\""))
                     ? value.substring(1, value.length() - 1)
                     : value;
    }

    public String getFieldName () {
        return fieldName;
    }

    public Operator getOperator () {
        return operator;
    }

    public String getValue () {
        return value;
    }

    @Override
    public Predicate evaluate (PathBuilder<?> pathBuilder) {
        PredicateBuilder predicateBuilder = PredicateFactory.createPredicateBuilder(fieldName, pathBuilder);
        return predicateBuilder.build(value, operator);
    }

    @Override
    public String toString () {
        return new StringBuilder()
                .append(fieldName)
                .append(' ')
                .append(operator.toString())
                .append(' ')
                .append(value)
                .toString();
    }

    @Override
    public int hashCode () {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.fieldName);
        hash = 13 * hash + Objects.hashCode(this.operator);
        hash = 13 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals (Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SingleCondition other = (SingleCondition) obj;
        if (!Objects.equals(this.fieldName, other.fieldName)) {
            return false;
        }
        if (this.operator != other.operator) {
            return false;
        }
        return Objects.equals(this.value, other.value);
    }
}
