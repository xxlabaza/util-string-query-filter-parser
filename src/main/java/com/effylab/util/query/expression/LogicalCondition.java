/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 22, 2014
 * <p>
 * @version 1.0.0
 */
public class LogicalCondition implements Condition {

    public static Pattern getExpressionStatementPattern () {
        return Pattern.compile('(' + LogicalOperator.getRegularExpressions("|") + ')');
    }

    private final Condition left;

    private final Condition right;

    private final LogicalOperator logicalOperator;

    public LogicalCondition (Condition left,
                             Condition right,
                             LogicalOperator logicalOperator) {
        this.left = left;
        this.right = right;
        this.logicalOperator = logicalOperator;
    }

    public Condition getLeft () {
        return left;
    }

    public Condition getRight () {
        return right;
    }

    public LogicalOperator getLogicalOperator () {
        return logicalOperator;
    }

    @Override
    public Predicate evaluate (PathBuilder<?> pathBuilder) {
        Predicate leftPredicate = left.evaluate(pathBuilder);
        Predicate rightPredicate = right.evaluate(pathBuilder);

        switch (logicalOperator) {
        case AND:
            return new BooleanBuilder(leftPredicate).and(rightPredicate);
        case OR:
            return new BooleanBuilder(leftPredicate).or(rightPredicate);
        case UNDEFINED:
        default:
            throw new UnsupportedOperationException("Unknown operator");
        }
    }

    @Override
    public String toString () {
        return new StringBuilder()
                .append(left != null
                        ? left.toString()
                        : "UNDEFINED")
                .append(' ')
                .append(logicalOperator != null
                        ? logicalOperator.toString()
                        : "UNDEFINED")
                .append(' ')
                .append(right != null
                        ? right.toString()
                        : "UNDEFINED")
                .toString();
    }

    @Override
    public int hashCode () {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.left);
        hash = 97 * hash + Objects.hashCode(this.right);
        hash = 97 * hash + Objects.hashCode(this.logicalOperator);
        return hash;
    }

    @Override
    public boolean equals (Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogicalCondition other = (LogicalCondition) obj;
        if (!Objects.equals(this.left, other.left)) {
            return false;
        }
        if (!Objects.equals(this.right, other.right)) {
            return false;
        }
        return this.logicalOperator == other.logicalOperator;
    }
}
