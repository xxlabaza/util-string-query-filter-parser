/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 22, 2014
 * <p>
 * @version 1.0.0
 */
public enum LogicalOperator {

    AND(Pattern.compile("\\s+and\\s+|^and$|\\s+AND\\s+|^AND$|&&")),
    OR(Pattern.compile("\\s+or\\s+|^or$|\\s+OR\\s+|^OR$|\\|\\|")),
    UNDEFINED;

    private final Pattern regularExpression;

    private LogicalOperator () {
        this(null);
    }

    private LogicalOperator (Pattern regularExpression) {
        this.regularExpression = regularExpression;
    }

    public Pattern getRegularExpression () {
        return regularExpression;
    }

    public static LogicalOperator parse (String logicalOperatorString) {
        if (logicalOperatorString == null) {
            return UNDEFINED;
        }
        String logicalOperatorName = logicalOperatorString.trim();
        if (logicalOperatorName.isEmpty()) {
            return UNDEFINED;
        }
        logicalOperatorName = logicalOperatorName.toLowerCase();
        for (LogicalOperator logicalOperator : values()) {
            Pattern pattern = logicalOperator.getRegularExpression();
            if ((pattern != null) && pattern.matcher(logicalOperatorName).matches()) {
                return logicalOperator;
            }
        }
        return UNDEFINED;
    }

    public static String getRegularExpressions (String delimeter) {
        StringBuilder sb = new StringBuilder();
        for (LogicalOperator logicalOperator : values()) {
            Pattern pattern = logicalOperator.getRegularExpression();
            if (pattern != null) {
                sb.append(pattern.pattern());
                sb.append(delimeter);
            }
        }
        sb.delete(sb.length() - delimeter.length(), sb.length());
        return sb.toString();
    }

    @Override
    public String toString () {
        return (regularExpression != null)
               ? regularExpression.pattern()
               : UNDEFINED.name();
    }
}
