/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 22, 2014
 * <p>
 * @version 1.0.0
 */
public enum Operator {

    GREATER_THAN_EQUALS(
            Pattern.compile("\\s+greater than equals\\s+|^greater than equals$|\\s*>=\\s*"),
            true
    ),
    LESS_THAN_EQUALS(
            Pattern.compile("\\s+less than equals\\s+|^less than equals$|\\s*<=\\s*"),
            true
    ),
    NOT_EQUAL_TO(
            Pattern.compile("\\s+not equal to\\s+|^not equal to$|\\s*!=\\s*"),
            true
    ),
    NOT_CONTAINS(
            Pattern.compile("\\s+not contains\\s+|^not contains$"),
            true
    ),
    LESS_THAN(
            Pattern.compile("\\s+less than\\s+|^less than$|\\s*<\\s*"),
            true
    ),
    GREATER_THAN(
            Pattern.compile("\\s+greater than\\s+|^greater than$|\\s*>\\s*"),
            true
    ),
    ENDS_WITH(
            Pattern.compile("\\s+ends with\\s+|^ends with$"),
            true
    ),
    STARTS_WITH(
            Pattern.compile("\\s+starts with\\s+|^starts with$"),
            true
    ),
    EQUALS(
            Pattern.compile("\\s+equals\\s+|^equals$|\\s*=\\s*"),
            true
    ),
    CONTAINS(
            Pattern.compile("\\s+contains\\s+|^contains$"),
            true
    ),
    IS_NULL(
            Pattern.compile("\\s+is null\\s*|^is null$"),
            false
    ),
    IS_NOT_NULL(
            Pattern.compile("\\s+is not null\\s*|^is not null$"),
            false
    ),
    UNDEFINED;

    private final Pattern regularExpression;

    private final boolean hasOperand;

    private Operator () {
        this(null, false);
    }

    private Operator (Pattern regularExpression, boolean hasOperand) {
        this.regularExpression = regularExpression;
        this.hasOperand = hasOperand;
    }

    public Pattern getRegularExpression () {
        return regularExpression;
    }

    public boolean hasOperand () {
        return hasOperand;
    }

    public static Operator parse (String operatorString) {
        if (operatorString == null) {
            return UNDEFINED;
        }
        String operatorName = operatorString.trim();
        if (operatorName.isEmpty()) {
            return UNDEFINED;
        }
        operatorName = operatorName.toLowerCase();
        for (Operator operator : values()) {
            Pattern pattern = operator.getRegularExpression();
            if ((pattern != null) && pattern.matcher(operatorName).matches()) {
                return operator;
            }
        }
        return UNDEFINED;
    }

    public static String getRegularExpressions (String delimeter) {
        StringBuilder sb = new StringBuilder();
        for (Operator operator : values()) {
            Pattern pattern = operator.getRegularExpression();
            if (pattern != null) {
                sb.append(pattern.pattern());
                sb.append(delimeter);
            }
        }
        sb.delete(sb.length() - delimeter.length(), sb.length());
        return sb.toString();
    }

    @Override
    public String toString () {
        return (regularExpression != null)
               ? regularExpression.pattern()
               : UNDEFINED.name();
    }
}
