/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.NullHandling;
import org.springframework.data.domain.Sort.Order;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 24, 2014
 * <p>
 * @version 1.0.0
 */
class SortParser {

    private static final Pattern DIRECTION_PATTERN;

    private static final Pattern NULL_HANDLER_PATTERN;

    static {
        DIRECTION_PATTERN = Pattern.compile("(asc|desc)", Pattern.CASE_INSENSITIVE);
        NULL_HANDLER_PATTERN = Pattern.compile("(nullsFirst|native|nullsLast)");
    }

    public Sort parse (String[] sort) {
        if (sort == null || sort.length == 0) {
            return null;
        }

        List<Order> orders = new ArrayList<>(sort.length);
        for (String orderString : sort) {
            Order order = createOrder(orderString);
            if (order != null) {
                orders.add(order);
            }
        }

        return new Sort(orders);
    }

    // Sort expression: id asc ignoreCase nullsFirst|native|nullsLast
    private Order createOrder (String orderString) {
        if (orderString == null) {
            return null;
        }
        String trimmedOrderString = orderString.trim();
        if (trimmedOrderString.isEmpty()) {
            return null;
        }

        String[] tokens = trimmedOrderString.split("\\s+");
        String property = tokens[0];
        if (tokens.length == 1) {
            return new Order(property);
        }

        Direction direction = Sort.Direction.ASC;
        boolean ignoreCase = false;
        NullHandling nullHandling = NullHandling.NATIVE;
        for (int i = 1; i < tokens.length; i++) {
            String token = tokens[i];
            if (token.equalsIgnoreCase("ignoreCase")) {
                ignoreCase = true;
            } else if (DIRECTION_PATTERN.matcher(token).matches()) {
                direction = Direction.fromString(token);
            } else if (NULL_HANDLER_PATTERN.matcher(token).matches()) {
                switch (token) {
                case "nullsFirst":
                    nullHandling = NullHandling.NULLS_FIRST;
                    break;
                case "native":
                    nullHandling = NullHandling.NATIVE;
                    break;
                case "nullsLast":
                    nullHandling = NullHandling.NULLS_LAST;
                    break;
                }
            }
        }
        Order order = new Order(direction, property, nullHandling);
        return ignoreCase
               ? order.ignoreCase()
               : order;
    }
}
