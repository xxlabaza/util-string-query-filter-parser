/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query;

import com.effylab.util.query.expression.Condition;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Filter object, which creates Predicate and Pageable objects for "find*" repository methods of spring-data.
 * <h2>Single conditions</h2>
 * <p>
 * <b>Single condition</b> - is a human-readable query string, which consist of three
 * parts:<blockquote><pre>&lt;fieldName&gt; &lt;operator&gt; [value]</pre></blockquote>
 * </p>
 * <p>
 * Examples:
 * <ul>
 * <li>count = 3</li>
 * <li>firstName starts with 'Andrew '</li>
 * <li>hashMap.key &lt; 811</li>
 * <li>list[4] &gt;= 0</li>
 * <li>booleanVariable = true</li>
 * <li>someInstance is not null</li>
 * </ul>
 * </p>
 * <h3>Field name</h3>
 * <p>
 * <b>fieldName</b> - is a mandatory token, represents a filtered object's field. Consists of: letters, digits and _, $
 * signs. Examples: <blockquote><pre>var, id, mySuperClassField13 and etc.</pre></blockquote>
 * </p>
 * <p>
 * Can be compound, if we talk about Map/Array/Object field, for instance: "myMap.itsKey" - it comes about map's key and
 * value by its key. Examples: <blockquote><pre>list[0], map.key and etc.</pre></blockquote>
 * </p>
 * <h3>Operator</h3>
 * <p>
 * <b>operator</b> - is a mandatory token, represents logical operator to condition. There are two kinds of operators:
 * with and without operand.
 * </p>
 * <h4>Available operators</h4>
 * <table border="0" cellpadding="1" cellspacing="0">
 * <tr align="left">
 * <th align="left">Alphabetical name&nbsp;</th>
 * <th align="left">Mathematical sign&nbsp;</th>
 * <th align="left">Has operand</th>
 * </tr>
 * <tr>
 * <td>equals&nbsp;</td>
 * <td>=</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>not equal to&nbsp;</td>
 * <td>!=</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>greater than equals&nbsp;</td>
 * <td>&gt;=</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>less than equals&nbsp;</td>
 * <td>&lt;=</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>less than&nbsp;</td>
 * <td>&lt;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>greater than&nbsp;</td>
 * <td>&gt;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>contains&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>not contains&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>ends with&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>starts with&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>yes</td>
 * </tr>
 * <tr>
 * <td>is null&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>no</td>
 * </tr>
 * <tr>
 * <td>is not null&nbsp;</td>
 * <td>&nbsp;</td>
 * <td>no</td>
 * </tr>
 * </table>
 * <h3>Value</h3>
 * <p>
 * <b>value</b> -is a optional token, which represents field value. The value may be enclosed in parentheses.
 * Examples: <blockquote><pre>1, true, popa, 'Hello world!', "Ola!" and etc.</pre></blockquote>
 * </p>
 * <h2>Logical conditions</h2>
 * <p>
 * <b>Logical conditions</b> - consist of two other single or logical conditions. For example:
 * <ul>
 * <li>someField = 0 and myString contains ' this_text '</li>
 * <li>flag = true or (object is null and index &lt; 7)</li>
 * </ul>
 * </p>
 * <p>
 * @see com.effylab.util.query.expression.SingleCondition
 * @see com.effylab.util.query.expression.LogicalCondition
 * <p>
 * @author Artem Labazin
 * </p>
 * <p>
 * @since Oct 22, 2014
 * </p>
 * <p>
 * @version 1.0.0
 * </p>
 */
public final class PageableFilter {

    private String filter;

    private String[] sort;

    private int page;

    private int size;

    /**
     * Sets page and size to default values: 0 and 25.
     */
    public PageableFilter () {
        page = 0;
        size = 25;
    }

    /**
     * Sets page and size to default values: 0 and 25.
     * <p>
     * @param filter query filter string.
     */
    public PageableFilter (String filter) {
        this();
        setFilter(filter);
    }

    /**
     * Sets page and size to default values: 0 and 25.
     * <p>
     * @param filter query filter string.
     * <p>
     * @param sort   sort options.
     */
    public PageableFilter (String filter, String[] sort) {
        this(filter);
        setSort(sort);
    }

    /**
     * Creates predicate by filter string and requesting object class.
     * <p>
     * @param klass requesting object class
     * <p>
     * @return filter predicate
     */
    public Predicate createPredicate (Class<?> klass) {
        if (filter == null || klass == null) {
            return null;
        }
        Condition condition = new FilterParser().parse(filter);
        PathBuilder<?> pathBuilder = new PathBuilder<>(klass, "entity");
        return condition.evaluate(pathBuilder);
    }

    /**
     * Creates pageable object by sorts strings.
     * <p>
     * @return pageable object
     */
    public Pageable createPageable () {
        Sort sorts = new SortParser().parse(sort);
        return (sorts == null)
               ? new PageRequest(page, size)
               : new PageRequest(page, size, sorts);
    }

    public String getFilter () {
        return filter;
    }

    /**
     * Sets filter string.
     * <p>
     * @param filter query filter string
     * <p>
     * @throws IllegalArgumentException if string is empty or null
     */
    public void setFilter (String filter) {
        if (filter == null || filter.trim().isEmpty()) {
            throw new IllegalArgumentException("Filter string is empty");
        }
        this.filter = filter.trim();
    }

    /**
     * Gets requesting page number.
     * <p>
     * @return page number
     */
    public int getPage () {
        return page;
    }

    /**
     * Sets requesting page number.
     * <p>
     * @param page page number
     * <p>
     * @throws IllegalArgumentException if page number less than 0
     */
    public void setPage (int page) {
        if (page < 0) {
            throw new IllegalArgumentException("Page must be greater or equal to 0");
        }
        this.page = page;
    }

    /**
     * Gets requesting page size.
     * <p>
     * @return page size
     */
    public int getSize () {
        return size;
    }

    /**
     * Sets requesting page size.
     * <p>
     * @param size page size
     * <p>
     * @throws IllegalArgumentException if page size less or equal to 0
     */
    public void setSize (int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Page size must be greater than 0");
        }
        this.size = size;
    }

    /**
     * Gets requesting page sorts.
     * <p>
     * @return array of request sort
     */
    public String[] getSort () {
        return sort;
    }

    /**
     * Sets requesting page sorts.
     * <p>
     * @param sort array of request sort
     */
    public void setSort (String[] sort) {
        this.sort = sort;
    }
}
