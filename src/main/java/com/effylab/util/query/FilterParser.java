/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query;

import com.effylab.util.query.expression.Condition;
import com.effylab.util.query.expression.LogicalCondition;
import com.effylab.util.query.expression.LogicalOperator;
import com.effylab.util.query.expression.SingleCondition;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Oct 22, 2014
 * <p>
 * @version 1.0.0
 */
class FilterParser {

    static final Pattern TOKEN_PATTERN;

    static {
        TOKEN_PATTERN = Pattern.compile("\\s*(" + "\\(" + // open bracket
                                        "|" + // or
                                        "\\)" + // close bracket
                                        "|" + // or
                                        SingleCondition.getExpressionStatementPattern().pattern() + // expression statement pattern
                                        "|" + // or
                                        LogicalCondition.getExpressionStatementPattern().pattern() + // logical operators
                                        ")"
        );
    }

    public Condition parse (String filter) {
        String preparedFilter = prepareFilter(filter);
        String[] tokens = splitFilterOnTokens(preparedFilter);
        String[] polishNotationTokens = convertInfixExpressionToPolishNotation(tokens);

        Stack<Condition> stack = new Stack<>();
        for (String token : polishNotationTokens) {
            if (isLogicalOperator(token)) {
                Condition right = stack.pop();
                Condition left = stack.pop();
                stack.push(new LogicalCondition(left, right, LogicalOperator.parse(token)));
            } else {
                stack.push(SingleCondition.parse(token));
            }
        }
        return stack.pop();
    }

    private String[] splitFilterOnTokens (String filter) {
        List<String> result = new ArrayList<>();
        Matcher matcher = TOKEN_PATTERN.matcher(filter);
        while (matcher.find()) {
            result.add(matcher.group(1));
        }
        return result.toArray(new String[result.size()]);
    }

    private String prepareFilter (String filter) {
        if (filter == null) {
            throw new IllegalArgumentException("Filter is null");
        }
        String trimmedFilter = filter.trim();
        if (trimmedFilter.isEmpty()) {
            throw new IllegalArgumentException("Filter is empty");
        }
        if (filter.contains("(") || filter.contains(")")) {
            // check parentheses balance
            int deepLevel = 0;
            for (char filterChar : filter.toCharArray()) {
                switch (filterChar) {
                case '(':
                    deepLevel++;
                    break;
                case ')':
                    deepLevel--;
                    break;
                }
            }
            if (deepLevel != 0) {
                throw new RuntimeException("Filter has incorrect parentheses balance");
            }
        }
        return trimmedFilter;
    }

    private String[] convertInfixExpressionToPolishNotation (String[] tokens) {
        List<String> result = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        for (String token : tokens) {
            if (isLogicalOperator(token)) {
                // token is a logical operator
                while (!stack.isEmpty() && isLogicalOperator(stack.peek())) {
                    result.add(stack.pop());
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                // add all stack items until '('
                while (!stack.isEmpty() && !stack.peek().equals("(")) {
                    result.add(stack.pop());
                }
                stack.pop();
            } else {
                // token is an expression
                result.add(token);
            }
        }
        while (!stack.isEmpty()) {
            result.add(stack.pop());
        }
        return result.toArray(new String[result.size()]);
    }

    private boolean isLogicalOperator (String token) {
        LogicalOperator logicalOperator = LogicalOperator.parse(token);
        return (logicalOperator != null) && !logicalOperator.equals(LogicalOperator.UNDEFINED);
    }
}
