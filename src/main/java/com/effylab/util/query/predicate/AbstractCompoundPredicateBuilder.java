/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.mysema.query.types.Expression;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 7, 2014
 * <p>
 * @version 1.0.1
 */
abstract class AbstractCompoundPredicateBuilder<T extends Expression<?>> extends AbstractSimplePredicateBuilder<T> {

    private static final Pattern MAP_OBJECT_FIELD_PATTERN;

    private static final Pattern ARRAY_FIELD_PATTERN;

    static {
        MAP_OBJECT_FIELD_PATTERN = Pattern.compile("(?<name>\\w+)\\.(?<key>[\\w\\.\\$]+)");
        ARRAY_FIELD_PATTERN = Pattern.compile("(?<name>\\w+)\\[(?<key>\\d+)\\]");
    }

    static String[] parseCompoundFieldName (String fieldName) {
        Matcher matcher = MAP_OBJECT_FIELD_PATTERN.matcher(fieldName);
        if (!matcher.matches()) {
            matcher = ARRAY_FIELD_PATTERN.matcher(fieldName);
            if (!matcher.matches()) {
                return null;
            }
        }
        return new String[] {matcher.group("name"), matcher.group("key")};
    }

    AbstractCompoundPredicateBuilder (T path) {
        super(path);
    }
}
