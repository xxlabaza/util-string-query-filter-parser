/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.effylab.util.query.expression.Operator;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.SimpleExpression;
import com.mysema.query.types.path.MapPath;
import com.mysema.query.types.path.PathBuilder;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 6, 2014
 * <p>
 * @version 1.0.0
 */
final class MapPredicateBuilder extends AbstractCompoundPredicateBuilder<SimpleExpression<?>> {

    public static MapPredicateBuilder createMapPredicateBuilder (PathBuilder<?> pathBuilder, String filedName) {
        String[] tokens = parseCompoundFieldName(filedName);
        String name = tokens[0];
        String key = tokens[1];
        MapPath path = pathBuilder.getMap(name, Object.class, Object.class);
        return new MapPredicateBuilder(path.get(key));
    }

    private MapPredicateBuilder (SimpleExpression<?> path) {
        super(path);
    }

    @Override
    public Predicate build (String value, Operator operator) {
        return buildSimple(value, operator);
    }
}
