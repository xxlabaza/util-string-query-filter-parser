/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.effylab.util.query.expression.Operator;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 6, 2014
 * <p>
 * @version 1.0.0
 */
final class NumberPredicateBuilder extends AbstractSimplePredicateBuilder<NumberPath<?>> {

    public static NumberPredicateBuilder createNumberPredicateBuilder (PathBuilder<?> pathBuilder, String filedName) {
        Class<?> type;
        try {
            type = pathBuilder.getType().getDeclaredField(filedName).getType();
        } catch (NoSuchFieldException | SecurityException ex) {
            throw new RuntimeException();
        }

        NumberPath<?> numberPath = null;
        if (Integer.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Integer.class);
        } else if (Long.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Long.class);
        } else if (Short.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Short.class);
        } else if (BigInteger.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, BigInteger.class);
        } else if (BigDecimal.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, BigDecimal.class);
        } else if (Float.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Float.class);
        } else if (Double.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Double.class);
        } else if (Byte.class.isAssignableFrom(type)) {
            numberPath = pathBuilder.getNumber(filedName, Byte.class);
        }

        return new NumberPredicateBuilder(numberPath);
    }

    private NumberPredicateBuilder (NumberPath<?> path) {
        super(path);
    }

    @Override
    public Predicate build (String value, Operator operator) {
        NumberPath path = getPath();
        Number number = parseNumber(value);
        switch (operator) {
        case EQUALS:
            return path.eq(number);
        case NOT_EQUAL_TO:
            return path.ne(number);
        case GREATER_THAN:
            return path.gt(number);
        case GREATER_THAN_EQUALS:
            return path.goe(number);
        case LESS_THAN:
            return path.lt(number);
        case LESS_THAN_EQUALS:
            return path.loe(number);
        case IS_NULL:
            return path.isNull();
        case IS_NOT_NULL:
            return path.isNotNull();
        default:
            return buildSimple(value, operator);
        }
    }
}
