/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.mysema.query.types.path.PathBuilder;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 6, 2014
 * <p>
 * @version 1.0.0
 */
public final class PredicateFactory {

    public static PredicateBuilder createPredicateBuilder (String fieldName, PathBuilder<?> pathBuilder) {
        FieldType fieldType = FieldType.parse(pathBuilder.getType(), fieldName);
        switch (fieldType) {
        case NUMBER:
            return NumberPredicateBuilder.createNumberPredicateBuilder(pathBuilder, fieldName);
        case STRING:
            return new StringPredicateBuilder(pathBuilder.getString(fieldName));
        case BOOLEAN:
            return new BooleanPredicateBuilder(pathBuilder.getBoolean(fieldName));
        case DATE:
            return DatePredicateBuilder.createDatePredicateBuilder(pathBuilder, fieldName);
        case OBJECT:
            return ObjectPredicateBuilder.createObjectPredicateBuilder(pathBuilder, fieldName);
        case MAP:
            return MapPredicateBuilder.createMapPredicateBuilder(pathBuilder, fieldName);
        case ARRAY:
            return ArrayPredicateBuilder.createArrayPredicateBuilder(pathBuilder, fieldName);
        case UNDEFINED:
        default:
            throw new IllegalArgumentException("Unknown field type");
        }
    }

    private PredicateFactory () {
    }
}
