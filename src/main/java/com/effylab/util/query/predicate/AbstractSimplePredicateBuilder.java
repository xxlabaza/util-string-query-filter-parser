/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.effylab.util.query.expression.Operator;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.SimpleExpression;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 7, 2014
 * <p>
 * @version 1.0.0
 */
abstract class AbstractSimplePredicateBuilder<T extends Expression<?>> implements PredicateBuilder {

    private final T path;

    AbstractSimplePredicateBuilder (T path) {
        this.path = path;
    }

    protected T getPath () {
        return path;
    }

    protected Predicate buildSimple (String value, Operator operator) {
        SimpleExpression expression = (SimpleExpression) path;
        switch (operator) {
        case EQUALS:
            Object parsedValue = parseValue(value);
            return expression.eq(parsedValue);
        case NOT_EQUAL_TO:
            parsedValue = parseValue(value);
            return expression.ne(parsedValue);
        case IS_NULL:
            return expression.isNull();
        case IS_NOT_NULL:
            return expression.isNotNull();
        default:
            throw new IllegalArgumentException();
        }
    }

    protected Object parseValue (String value) {
        Object result = parseNumber(value);
        if (result != null) {
            return result;
        }
        result = parseDate(value);
        if (result != null) {
            return result;
        }
        return value;
    }

    protected Boolean parseBoolean (String value) {
        if ("yes".equalsIgnoreCase(value)) {
            return Boolean.TRUE;
        }
        return Boolean.parseBoolean(value);
    }

    protected Number parseNumber (String value) {
        Class<?> type = path.getType();
        try {
            if (Integer.class.isAssignableFrom(type)) {
                return Integer.parseInt(value);
            } else if (Long.class.isAssignableFrom(type)) {
                return Long.parseLong(value);
            } else if (Short.class.isAssignableFrom(type)) {
                return Short.parseShort(value);
            } else if (BigInteger.class.isAssignableFrom(type)) {
                return new BigInteger(value);
            } else if (BigDecimal.class.isAssignableFrom(type)) {
                return new BigDecimal(value);
            } else if (Float.class.isAssignableFrom(type)) {
                return Float.parseFloat(value);
            } else if (Double.class.isAssignableFrom(type)) {
                return Double.parseDouble(value);
            } else if (Byte.class.isAssignableFrom(type)) {
                return Byte.parseByte(value);
            } else {
                return Integer.parseInt(value);
            }
        } catch (NumberFormatException ex) {
            // throw
        }
        return null;
    }

    protected Date parseDate (String value) {
        try {
            long longValue = Long.parseLong(value);
            return new Date(longValue);
        } catch (NumberFormatException ex) {

        }
        return null;
    }
}
