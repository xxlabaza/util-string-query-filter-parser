/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * <p>
 * @author Artem Labazin (xxlabaza@gmail.com)
 * <p>
 * @since Feb 4, 2015 | 5:45:18 AM
 * <p>
 * @version 1.0.0
 */
enum FieldType {

    NUMBER,
    STRING,
    BOOLEAN,
    DATE,
    ARRAY,
    MAP,
    OBJECT,
    UNDEFINED;

    public static FieldType parse (Class<?> klass, String fieldName) {
        if (klass == null || fieldName == null) {
            return UNDEFINED;
        }

        String preparedFieldName = fieldName.trim();
        String[] tokens = AbstractCompoundPredicateBuilder.parseCompoundFieldName(preparedFieldName);
        if (tokens != null) {
            preparedFieldName = tokens[0];
        }

        Class<?> fieldClass;
        try {
            fieldClass = klass.getDeclaredField(preparedFieldName).getType();
        } catch (NoSuchFieldException | SecurityException ex) {
            return UNDEFINED;
        }

        if (Number.class.isAssignableFrom(fieldClass)) {
            return NUMBER;
        } else if (String.class.isAssignableFrom(fieldClass)) {
            return STRING;
        } else if (Boolean.class.isAssignableFrom(fieldClass)) {
            return BOOLEAN;
        } else if (Date.class.isAssignableFrom(fieldClass)) {
            return DATE;
        } else if (Map.class.isAssignableFrom(fieldClass)) {
            return MAP;
        } else if (List.class.isAssignableFrom(fieldClass) || fieldClass.isArray()) {
            return ARRAY;
        }

        return OBJECT;
    }
}
