/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.effylab.util.query.expression.Operator;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.SimpleExpression;
import com.mysema.query.types.path.ListPath;
import com.mysema.query.types.path.PathBuilder;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 7, 2014
 * <p>
 * @version 1.0.0
 */
final class ArrayPredicateBuilder extends AbstractCompoundPredicateBuilder<SimpleExpression<?>> {

    public static ArrayPredicateBuilder createArrayPredicateBuilder (PathBuilder<?> pathBuilder, String filedName) {
        String[] tokens = parseCompoundFieldName(filedName);
        String name = tokens[0];
        Integer index = Integer.parseInt(tokens[1]);
        ListPath path = pathBuilder.getList(name, Object.class);
        return new ArrayPredicateBuilder(path.get(index));
    }

    private ArrayPredicateBuilder (SimpleExpression<?> path) {
        super(path);
    }

    @Override
    public Predicate build (String value, Operator operator) {
        return buildSimple(value, operator);
    }
}
