/*
 * The MIT License
 *
 * Copyright 2015 Artem Labazin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.predicate;

import com.effylab.util.query.expression.Operator;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.StringPath;

/**
 *
 * <p>
 * @author Artem Labazin
 * <p>
 * @since Nov 6, 2014
 * <p>
 * @version 1.0.0
 */
final class StringPredicateBuilder extends AbstractSimplePredicateBuilder<StringPath> {

    StringPredicateBuilder (StringPath path) {
        super(path);
    }

    @Override
    public Predicate build (String value, Operator operator) {
        StringPath path = getPath();
        switch (operator) {
        case CONTAINS:
            return path.contains(value);
        case NOT_CONTAINS:
            return path.notLike(value);
        case STARTS_WITH:
            return path.startsWith(value);
        case ENDS_WITH:
            return path.endsWith(value);
        default:
            return buildSimple(value, operator);
        }
    }
}
