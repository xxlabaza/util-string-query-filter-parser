/*
 * The MIT License
 *
 * Copyright 2015 xxlabaza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query;

import com.effylab.util.query.expression.Condition;
import com.effylab.util.query.expression.LogicalCondition;
import com.effylab.util.query.expression.LogicalOperator;
import com.effylab.util.query.expression.Operator;
import com.effylab.util.query.expression.SingleCondition;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author xxlabaza
 */
public class FilterParserTest {

    private static final FilterParser FILTER_PARSER;

    private static final Map<String, Condition> VALID_FILTERS;

    private static final String[] INVALID_FILTERS;

    static {
        FILTER_PARSER = new FilterParser();

        Map<String, Condition> filters = new HashMap<>(4);
        filters.put("field = 4", new SingleCondition("field", Operator.EQUALS, "4"));
        filters.put("field >= 1 and variable not equal to 'Hello world'",
                    new LogicalCondition(
                            new SingleCondition("field", Operator.GREATER_THAN_EQUALS, "1"),
                            new SingleCondition("variable", Operator.NOT_EQUAL_TO, "Hello world"),
                            LogicalOperator.AND
                    )
        );
        filters.put("(field less than 23425 or field greater than 9999999) && some_string ends with 'popa'",
                    new LogicalCondition(
                            new LogicalCondition(
                                    new SingleCondition("field", Operator.LESS_THAN, "23425"),
                                    new SingleCondition("field", Operator.GREATER_THAN, "9999999"),
                                    LogicalOperator.OR
                            ),
                            new SingleCondition("some_string", Operator.ENDS_WITH, "popa"),
                            LogicalOperator.AND
                    )
        );
        filters.put(
                "(field > 1 || (variable equals 7 and some is null)) && (field2 < 17 or field3 is not null) and some_string starts with \"123.\"",
                new LogicalCondition(
                        new LogicalCondition(
                                new LogicalCondition(
                                        new SingleCondition("field", Operator.GREATER_THAN, "1"),
                                        new LogicalCondition(
                                                new SingleCondition("variable", Operator.EQUALS, "7"),
                                                new SingleCondition("some", Operator.IS_NULL),
                                                LogicalOperator.AND
                                        ),
                                        LogicalOperator.OR
                                ),
                                new LogicalCondition(
                                        new SingleCondition("field2", Operator.LESS_THAN, "17"),
                                        new SingleCondition("field3", Operator.IS_NOT_NULL),
                                        LogicalOperator.OR
                                ),
                                LogicalOperator.AND
                        ),
                        new SingleCondition("some_string", Operator.STARTS_WITH, "123."),
                        LogicalOperator.AND
                )
        );
        VALID_FILTERS = Collections.unmodifiableMap(filters);

        INVALID_FILTERS = new String[] {
            "",
            "field",
            "=",
            "less than",
            "field % 4",
            "field has 1",
            "field is 5"
        };
    }

    @Test
    public void test_TOKEN_PATTERN_Valid () {
        for (String filter : VALID_FILTERS.keySet()) {
            Matcher matcher = FilterParser.TOKEN_PATTERN.matcher(filter);
            Assert.assertTrue("Filter: " + filter, matcher.find());
        }
    }

    @Test
    public void test_TOKEN_PATTERN_Invalid () {
        for (String filter : INVALID_FILTERS) {
            Matcher matcher = FilterParser.TOKEN_PATTERN.matcher(filter);
            Assert.assertFalse("Filter: " + filter, matcher.find());
        }
    }

    @Test
    public void test_parse_Valid () {
        for (Map.Entry<String, Condition> entry : VALID_FILTERS.entrySet()) {
            Condition expectation = VALID_FILTERS.get(entry.getKey());

            Condition builder = FILTER_PARSER.parse(entry.getKey());
            assertCondition("Filter: " + entry.getKey(), expectation, builder);
        }
    }

    @Test(expected = RuntimeException.class)
    public void test_parse_Invalid () {
        for (String filter : INVALID_FILTERS) {
            Condition builder = FILTER_PARSER.parse(filter);
            Assert.assertNull("Filter: " + filter, builder);
        }
    }

    private void assertCondition (String message, Condition expectation, Condition condition) {
        Assert.assertNotNull(message, expectation);
        Assert.assertNotNull(message, condition);

        if (condition instanceof SingleCondition) {
            assertSingleCondition(message, expectation, (SingleCondition) condition);
        } else if (condition instanceof LogicalCondition) {
            assertLogicalCondition(message, expectation, (LogicalCondition) condition);
        } else {
            Assert.fail("Unknown statement instance: \"" + condition.getClass() + "\". " + message);
        }
    }

    private void assertSingleCondition (String message, Condition expectation, SingleCondition condition) {
        Assert.assertTrue(message, expectation instanceof SingleCondition);
        SingleCondition expectationCondition = (SingleCondition) expectation;
        Assert.assertEquals(message, expectationCondition.getFieldName(), condition.getFieldName());
        Assert.assertEquals(message, expectationCondition.getOperator(), condition.getOperator());
        Assert.assertEquals(message, expectationCondition.getValue(), condition.getValue());
        Assert.assertEquals(message, expectationCondition, condition);
    }

    private void assertLogicalCondition (String message, Condition expectation, LogicalCondition condition) {
        Assert.assertTrue(message, expectation instanceof LogicalCondition);
        LogicalCondition expectationCondition = (LogicalCondition) expectation;
        assertCondition(message, expectationCondition.getLeft(), condition.getLeft());
        assertCondition(message, expectationCondition.getRight(), condition.getRight());
        Assert.assertEquals(message, expectationCondition.getLogicalOperator(), condition.getLogicalOperator());
    }
}
