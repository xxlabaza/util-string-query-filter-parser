/*
 * The MIT License
 *
 * Copyright 2015 xxlabaza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * <p>
 * @author Artem Labazin (xxlabaza@gmail.com)
 * <p>
 * @date Feb 4, 2015 3:22:23 PM
 * <p>
 * @version 1.0
 */
public class OperatorTest {

    private static final Map<Operator, String[]> OPERATORS;

    static {
        Map<Operator, String[]> values = new HashMap<>();
        values.put(Operator.GREATER_THAN_EQUALS,
                   new String[] {"greater than equals", " greater than equals ", ">=", " >=", ">= ", " >= "});
        values.put(Operator.LESS_THAN_EQUALS,
                   new String[] {"less than equals", " less than equals ", "<=", " <=", "<= ", " <= "});
        values.put(Operator.NOT_EQUAL_TO,
                   new String[] {"not equal to", " not equal to ", "!=", " !=", "!= ", " != "});
        values.put(Operator.NOT_CONTAINS,
                   new String[] {"not contains", " not contains "});
        values.put(Operator.LESS_THAN,
                   new String[] {"less than", " less than ", "<", " <", "< ", " < "});
        values.put(Operator.GREATER_THAN,
                   new String[] {"greater than", " greater than ", ">", " >", "> ", " > "});
        values.put(Operator.ENDS_WITH,
                   new String[] {"ends with", " ends with "});
        values.put(Operator.STARTS_WITH,
                   new String[] {"starts with", " starts with "});
        values.put(Operator.EQUALS,
                   new String[] {"equals", " equals ", "=", " =", "= ", " = "});
        values.put(Operator.CONTAINS,
                   new String[] {"contains", " contains "});
        values.put(Operator.IS_NULL,
                   new String[] {"is null", " is null "});
        values.put(Operator.IS_NOT_NULL,
                   new String[] {"is not null", " is not null "});
        OPERATORS = Collections.unmodifiableMap(values);
    }

    @Test
    public void parse_Correct () {
        for (Map.Entry<Operator, String[]> entry : OPERATORS.entrySet()) {
            for (String operatorString : entry.getValue()) {
                Operator operator = Operator.parse(operatorString);

                Assert.assertNotNull("Operator string: " + operatorString, operator);
                Assert.assertEquals("Operator string: " + operatorString, entry.getKey(), operator);
            }
        }
    }
}
