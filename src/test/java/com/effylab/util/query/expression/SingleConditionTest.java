/*
 * The MIT License
 *
 * Copyright 2015 xxlabaza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.effylab.util.query.expression;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * <p>
 * @author Artem Labazin (xxlabaza@gmail.com)
 * <p>
 * @since Feb 4, 2015 | 12:50:16 AM
 * <p>
 * @version 1.0.0
 */
public class SingleConditionTest {

    private static final Map<String, SingleCondition> conditions;

    static {
        Map<String, SingleCondition> values = new HashMap<>();
        values.put("name equals value",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name = value",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name not equal to value",
                   new SingleCondition("name", Operator.NOT_EQUAL_TO, "value"));
        values.put("name != value",
                   new SingleCondition("name", Operator.NOT_EQUAL_TO, "value"));
        values.put("name less than value",
                   new SingleCondition("name", Operator.LESS_THAN, "value"));
        values.put("name < value",
                   new SingleCondition("name", Operator.LESS_THAN, "value"));
        values.put("name less than equals value",
                   new SingleCondition("name", Operator.LESS_THAN_EQUALS, "value"));
        values.put("name <= value",
                   new SingleCondition("name", Operator.LESS_THAN_EQUALS, "value"));
        values.put("name greater than value",
                   new SingleCondition("name", Operator.GREATER_THAN, "value"));
        values.put("name > value",
                   new SingleCondition("name", Operator.GREATER_THAN, "value"));
        values.put("name greater than equals value",
                   new SingleCondition("name", Operator.GREATER_THAN_EQUALS, "value"));
        values.put("name >= value",
                   new SingleCondition("name", Operator.GREATER_THAN_EQUALS, "value"));
        values.put("name contains value",
                   new SingleCondition("name", Operator.CONTAINS, "value"));
        values.put("name not contains value",
                   new SingleCondition("name", Operator.NOT_CONTAINS, "value"));
        values.put("name starts with value",
                   new SingleCondition("name", Operator.STARTS_WITH, "value"));
        values.put("name ends with value",
                   new SingleCondition("name", Operator.ENDS_WITH, "value"));
        values.put("name is null",
                   new SingleCondition("name", Operator.IS_NULL));
        values.put("name is not null",
                   new SingleCondition("name", Operator.IS_NOT_NULL));
        values.put("name equals 'value'",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name = 'value'",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name= 'value'",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name ='value'",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name equals \"value\"",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name = \"value\"",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name= \"value\"",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name =\"value\"",
                   new SingleCondition("name", Operator.EQUALS, "value"));
        values.put("name = \"Артем\"",
                   new SingleCondition("name", Operator.EQUALS, "Артем"));
        conditions = Collections.unmodifiableMap(values);
    }

    @Rule
    public final ExpectedException exception;

    public SingleConditionTest () {
        exception = ExpectedException.none();
    }

    @Test
    public void parse_Correct () {
        for (Map.Entry<String, SingleCondition> entry : conditions.entrySet()) {
            SingleCondition condition = SingleCondition.parse(entry.getKey());
            Assert.assertNotNull("Condition: " + entry.getKey(), condition);
            assertCondition("Condition: " + entry.getKey(), entry.getValue(), condition);
        }
    }

    @Test
    public void parse_Incorrect_TooManyElementsInString () {
        String expressionString = "name equals value anotherValue";

        exception.expect(RuntimeException.class);
        exception.expectMessage("Filter expression string, \"" + expressionString + "\", doesn't match pattern");

        SingleCondition.parse(expressionString);
    }

    @Test
    public void parse_Incorrect_TooLowElementsInString () {
        String expressionString = "name equals";

        exception.expect(RuntimeException.class);
        exception.expectMessage("Filter expression string, \"" + expressionString + "\", doesn't match pattern");

        SingleCondition.parse(expressionString);
    }

    @Test
    public void parse_Incorrect_UnknownOperatorOperator () {
        String expressionString = "name operator_must_be_here value";

        exception.expect(RuntimeException.class);
        exception.expectMessage("Filter expression string, \"" + expressionString + "\", doesn't match pattern");

        SingleCondition.parse(expressionString);
    }

    @Test
    public void parse_Incorrect_NullExpressionString () {
        exception.expect(RuntimeException.class);
        exception.expectMessage("Filter expression string is null");

        SingleCondition.parse(null);
    }

    @Test
    public void parse_Incorrect_EmptyExpressionString () {
        exception.expect(RuntimeException.class);
        exception.expectMessage("Filter expression string is empty");

        SingleCondition.parse("");
    }

    private void assertCondition (String message, SingleCondition expectation, SingleCondition condition) {
        String fieldName = condition.getFieldName();
        Assert.assertNotNull(message, fieldName);
        Assert.assertEquals(message, fieldName, expectation.getFieldName());

        Operator operator = condition.getOperator();
        Assert.assertNotNull(message, operator);
        Assert.assertEquals(message, operator, expectation.getOperator());

        String value = condition.getValue();
        Assert.assertNotNull(message, value);
        Assert.assertEquals(message, value, expectation.getValue());

        Assert.assertEquals(message, expectation, condition);
    }
}
